# Partie 1

## Sous-partie 1 : texte

Une phrase sans rien

*Une phrase en italique*

**Une phrase en gras**

Un lien vers [fun-mooc.fr](https://www.fun-mooc.fr/)

Une ligne de `code`

## Sous-partie 2 : listes

**Liste à puce**
- iteml
  - sous-item
  - sous-item
- iteml
- iteml

**Liste numérotée**
1. item
2. item
3. item

## Sous-partie 3 : cod
```
# Extrait de code
```
